/**
 * Require ES6 to help plugins - until Node is updated
 */
require('es6-promise').polyfill();


/**
 * Cnfg variables - declares gloabl paths which then get merged with predefined `global.config` ones
 * @example

global.config {
	'paths' {
		'src': 'new/path'
	}
}

 */
var cnfg = {
	'paths': {
		'regex': '/.*(?=/build)/',
		'src': 'build/',
		'dest': 'public_html/assets/',

		'images': 'build/*/img/',
		'scripts': 'build/*/js/',
		'styles': 'build/*/css/',
		'sprite': 'build/*/sprite/',
		'files': [
			'build/**/*',
			'!build/**/.*',
			'!build/*/css{,/**}',
			'!build/*/js{,/**}',
			'!build/*/img{,/**}',
			'!build/*/sprite{,/**}',
			'!build/**/*~'
		],

		'templates': {
			'scssLint': './tpl/lint-config.yml',
			'svgSprite': {
				'cssDest': './',
				'cssSprite': 'img/sprite.svg',
				'scssDest': 'css/src/_sprite.scss',
				'scssTemplate': './tpl/gulp-svg-sprite.scss',
			}
		},
		'output': {
			'scssLint': './tpl/output.json'
		}
	},
	'imageExtensions': '{gif,jpg,png,svg,ico}',
	'flags': {
		'isProduction': true,
		'isQA': false
	}
};
var merge = require('merge');
config = merge.recursive(true, cnfg, global.config);

// Include gulp and plugins
var gulp = require('gulp');
var $ = {
	// Npm plugins
	del: require('del'),
	glob: require('glob'),

	// Gulp plugins
	changed: require('gulp-changed'),
	combineMediaQueries: require('gulp-combine-media-queries'),
	cssmin: require('gulp-minify-css'),
	debug: require('gulp-debug'),
	gsync: require('gulp-sync')(gulp),
	gutil: require('gulp-util'),
	imagemin: require('gulp-imagemin'),
	include: require('gulp-include'),
	postcss: require('gulp-postcss'),
	rename: require('gulp-rename'),
	sass: require('gulp-sass'),
	scssLint: require('gulp-scss-lint'),
	size: require('gulp-size'),
	svgSprite: require('gulp-svg-sprite'),
	uglify: require('gulp-uglify'),

	// PostCSS plugins
	autoprefixer: require('autoprefixer'),
	fallbackRoundSubpixels: require('postcss-round-subpixels'),
	postcssAssets: require('postcss-assets'),
	postcssRucksack: require('rucksack-css'),
	postNotify: require('postcss-browser-reporter')
}

// Set development flag
if($.gutil.env.dev === true)
	config.flags.isProduction = false;

// Set QA flag
if($.gutil.env.qa === true)
	config.flags.isQA = true;

/**
* Functions
*/
/**
 * @description Change event function - makes more consistent
 * @param {Event} evt - event from plugin
 * @returns {Output} Logs to terminal
 */
var changeEvent = function(evt) {
	$.gutil.log('File', $.gutil.colors.cyan(evt.path.replace(new RegExp(config.paths.regex), '')), 'was', $.gutil.colors.magenta(evt.type));
};

/**
 * @description Error event, builds up user friendly error message
 * @param {Error} err - error from plugin
 * @returns {Output} Logs to terminal
 */
var errorEvent = function(err) {
	var message = err.message;

	if(err.hasOwnProperty('file'))
		message = message  + ' in file ' + $.gutil.colors.cyan(err.file.replace(new RegExp(config.paths.regex), ''));

	if(err.hasOwnProperty('line'))
		message = message  + ' on line ' + $.gutil.colors.cyan(err.line);
	else if (err.hasOwnProperty('lineNumber'))
		message = message  + ' on line ' + $.gutil.colors.cyan(err.lineNumber);

	$.gutil.log($.gutil.colors.red('[problem]'), message);
};

/**
 * @description Finds global "src" in string and replaces with global "dest"
 * @param {String} path
 * @returns {String} Replaced path
 */
function srcToDest(path) {
	return path.replace(config.paths.src, config.paths.dest);
};

/**
 * @description Generate a src & dest object from a single path up until <keyword>
 * @param {String} e - the path string
 * @param {String} keyword - The asset keyword (e.g. js or css)
 * @returns {Object} Contains corrected src and dest paths until <keyword>
 * @example
	e = /home/user/www/client/project/build/project/img/image.jpg
	keyword = img
	output = {
		src = /build/project/img/
		dest = /public_html/asset/project/img/
	}
 */
function getPaths(e, keyword) {
	var path = e.path.match('/(.*' + keyword + ')/');
	var src = path[0];
	var dest = srcToDest(src);

	return {
		src: src,
		dest: dest
	}
};

/**
 * @description Lints SCSS files
 * @returns {Output} Logs to console with errors
 * @returns {File} Writes all errors found to file
 */
function scssLint() {
	var myCustomReporter = function(file) {
		if (!file.scsslint.success) {
			$.gutil.log(
				$.gutil.colors.yellow('[lint warning] ')
				+ $.gutil.colors.cyan(file.scsslint.issues.length)
				+ ' issues found in '
				+ $.gutil.colors.cyan(file.relative)
			);
		}

	};

	return gulp.src([
		config.paths.styles + '**/*.scss',
		'!' + config.paths.styles + 'src/*'
	])
	.pipe($.scssLint({
		config : config.paths.templates.scssLint,
		maxBuffer: 999999999,
		reporterOutput: config.paths.output.scssLint,
		customReport: myCustomReporter
	}))
}

/**
 * @description Compiles SCSS files to CSS
 * @param {String} src - The source folder
 * @param {String} dest - The destination folder
 * @param {Boolean} isQA - If the QA flag is active, it will lint scss files on compile
 * @param {Boolean} isProduction - If the Production flag is active, don't minify CSS or combineMediaQueries
 *
 * The CSS gets piped through:
 * - sass - compiles SCSS to CSS
 * - postcss
 *   - Rucksack (https://simplaio.github.io/rucksack/)
 *   - Autoprefixer (https://github.com/postcss/autoprefixer)
 *   - Round Subpixels (https://github.com/himynameisdave/postcss-round-subpixels)
 *   - Browser Notify (https://github.com/postcss/postcss-browser-reporter)
 * - combineMediaQueries - Combines media queries from Sass [if isProduction flag is set to true]
 * - cssmin - Minifies CSS [if isProduction flag is set to true]
 * - size -lists size of each CSS file
 */
function css(src, dest) {
	if(config.flags.isQA)
		scssLint();

	return gulp.src(src + '*.scss')
		.pipe($.sass({
			precision: 3,
			errLogToConsole: false,
			onError: function(err){
				errorEvent(err)
			}
		}))
		.on('error', errorEvent)
		.pipe($.postcss([
			$.postcssRucksack(),
			$.autoprefixer({
				'browsers': ['last 2 version']
			}),
			$.fallbackRoundSubpixels,
			$.postNotify
		]))
		.on('error', errorEvent)
		.pipe(config.flags.isProduction ? $.combineMediaQueries({
			'log': true
		}) : $.gutil.noop())
		.on('error', errorEvent)
		.pipe(config.flags.isProduction ? $.cssmin({
			advanced: false,
			keepBreaks: false,
			keepSpecialComments: 0,
			shorthandCompacting: false,
			processImport: false,
			aggressiveMerging: false
		}) : $.gutil.noop())
		.on('error', errorEvent)
		.pipe($.size({
			'showFiles': true
		}))
		.pipe(gulp.dest(dest))
}

/**
 * @description Process and Minify JS
 * @param {String} src - The source folder
 * @param {String} dest - The destination folder
 * @param {Boolean} isProduction - If the Product flag is active, don't uglify JS
 *
 * The JS gets piped through:
 * - include - to include any other JS files (https://www.npmjs.com/package/gulp-include)
 * - uglify [if isProduction flag is set to true]
 * - size - lists size of each JS file
 */
function js(src, dest) {

	return gulp.src([
			src + '**/*.js',
			'!' + src + '**/_*.js'
		])
		.pipe($.include())
		.pipe((config.flags.isProduction) ? $.uglify() : $.gutil.noop())
		.on('error', errorEvent)
		.pipe($.size({
			'showFiles': true
		}))
		.pipe(gulp.dest(dest));
}

/**
 * @description Create an SVG sprite & CSS
 * @param {String} src - The source folder
 * @param {String} dest - The destination folder
 *
 * The SVGs gets piped through:
 * - rename - To take `icons_` off the front of each icon name (Illustrator generates this)
 * - svgSprite - This generates a sprite.svg and _sprite.scss. See sass docs for usage
 */
function svgSprite(src, dest) {
	return gulp.src(src + '*.svg')
		.pipe($.rename(function(path){
			path.basename = path.basename.replace('icons_', '');
		}))
		.pipe($.svgSprite({
			shape: {
				spacing: {
					padding: [0, 0, 10, 0]
				}
			},
			mode: {
				css: {
					dest: config.paths.templates.svgSprite.cssDest,
					layout: 'vertical',
					sprite: config.paths.templates.svgSprite.cssSprite,
					bust: false,
					render: {
						scss: {
							dest: config.paths.templates.svgSprite.scssDest,
							template: config.paths.templates.svgSprite.scssTemplate
						}
					}
				}
			},
			variables: {
				mapname: 'icons',
				timestamp: Math.floor(Date.now() / 1000)
			}
		}))
		.on('error', errorEvent)
		.pipe(gulp.dest(dest))
}

/**
 * @description Minify images
 * @param {String} src - The source folder
 * @param {String} dest - The destination folder
 *
 * The Images gets piped through:
 * - changed - this only passes through images that have changed
 * - imagemin - minifies the images
 * - size - lists size of each image file
 */
function images(src, dest) {
	return gulp.src([
			src + '**/*.' + config.imageExtensions,
			'!' + src + '**/._*.' + config.imageExtensions
		])
		.pipe($.changed(dest, {
			hasChanged: $.changed.compareSha1Digest
		}))
		.pipe($.imagemin())
		.on('error', errorEvent)
		.pipe($.size({
			showFiles: true
		}))
		.pipe(gulp.dest(dest))
}

/**
 * @description Copys any files that are not in a js, css, img or sprite folder.
 * Will also not copy over files that begin with a . or ~
 */
function files() {
	return gulp.src(config.paths.files)
		.pipe(gulp.dest(config.paths.dest))
}

/**
 * @description Deletes all files and folders in destination (for recompile)
 * @augments Gulp
 */
gulp.task('del', function(){
	var dirs = [config.paths.dest + '*'];
	if(global.additionalDirs)
		dirs = dirs.concat(global.additionalDirs)

	return $.del.sync(dirs);
});

/**
 * @description Global watch task
 * @augments Gulp
 */
gulp.task('watch', function(){

	/* Watches for anything which is not an "asset" */
	gulp.watch(config.paths.files, function(e){
		if(e.type == 'deleted') {
			return $.del(srcToDest(e.path))
		} else {
			return files();
		}
	}).on('change', changeEvent);

	/* Watches sprite icons & recompiles sprite & CSS partial */
	gulp.watch(config.paths.sprite + '*', function(e){
		var p = getPaths(e, 'sprite');
		return svgSprite(p.src, p.src.replace('sprite/', ''))
	}).on('change', changeEvent);

	/* Watches SCSS sheets and recompiles all CSS in that "theme" */
	gulp.watch(config.paths.styles + '**/*.scss', function(e){
		var p = getPaths(e, 'css');
		return css(p.src, p.dest);
	}).on('change', changeEvent);

	/* Watches image changes for that theme & minifies if changed, deletes if removed */
	gulp.watch(config.paths.images + '**/*.' + config.imageExtensions, function(e){
		if(e.type == 'deleted') {
			return $.del(srcToDest(e.path))
		} else {
			var p = getPaths(e, 'img');
			return images(p.src, p.dest);
		}
	}).on('change', changeEvent);

	/* Watches JS changes for that theme & uglifies if changed */
	gulp.watch(config.paths.scripts + '**/*.js', function(e){
		var p = getPaths(e, 'js');
		return js(p.src, p.dest);
	}).on('change', changeEvent);
});

/**
 * @description Compile task to recompile all the assets
 * @augments Gulp
 */
gulp.task('compile', ['del'], function() {
	/* Find all sprite files and recompile */
	var spriteFiles = $.glob.sync(config.paths.sprite);
	for (var i = 0; i < spriteFiles.length; i++) {
		var dest = spriteFiles[i].replace('sprite/', '');
		return svgSprite(spriteFiles[i], dest);
	};

	/* Find all CSS files and trigger CSS function for each */
	var styleFiles = $.glob.sync(config.paths.styles)
	for (var i = 0; i < styleFiles.length; i++) {
		css(styleFiles[i], srcToDest(styleFiles[i]));
	};

	/* Find all JS files and trigger CSS function for each */
	var scriptFiles = $.glob.sync(config.paths.scripts);
	for (var i = 0; i < scriptFiles.length; i++) {
		js(scriptFiles[i], srcToDest(scriptFiles[i]));
	};

	/* Compile images */
	images(config.paths.images, config.paths.dest);

	/* Copy remaining files */
	files();
});
